<!DOCTYPE html>
<html>
<head>
	<title>Trip Or Trick ไปเที่ยวด้วยกันนะ</title>
	<!--Import Google Icon Font-->
    <link href='https://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet' type='text/css'>
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
    <link rel="shortcut icon" href="imgs/world.ico">

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" type="text/css" href="css/style.css">

</head>
<body class="grey lighten-4">
	<!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <script src="js/scripts.js"></script>
    <!--<div class="loader"></div>-->
    <?php 
        session_start(); 
        include('phpScripts.php');
        if(isset($_GET['status'])){
          checkToastStatus($_GET['status']);
        }
    ?>
    <!-- Code Start -->
	  <!-- Page Start -->
   <div class="my-container">
      	<!-- Navbar --> 
      	<?php
        	showNav(); 
	        showSlider();
      	?>
   		<!-- New Review -->
   		<!-- First Row -->
   		<a href="show.php?news=hots">
   			<div class="row red z-depth-1 index-show-topic">
          		<i class="material-icons medium left white-text" style="margin-right:0px;">grade</i>
   				    <h4 class="white-text left topic">Hot Reviews</h4><h5 class="left white-text hide-on-med-and-down second-topic">รีวิวที่ได้รับความสนใจ</h5>
   				    <i class="material-icons right medium circle red darken-1 white-text hide-on-med-and-down">keyboard_arrow_right</i>
   			</div>
   		</a>

   		<div class="row red lighten-1 center z-depth-1" style="padding-bottom: 5px;">
        <?php showHotReviews(); ?>
   		</div>

   	<!-- Second Row -->
   		<a href="show.php?news=news&p=1">
   			<div class="row purple z-depth-1 index-show-topic ">
          <i class="material-icons medium left white-text" style="margin-right:0px;">location_on</i>
   				<h4 class="white-text left topic">New Reviews</h4><h5 class="left white-text hide-on-med-and-down second-topic">รวบรวมรีวิวใหม่ๆจากสมาชิกของเรา</h5>
   				<i class="material-icons right medium purple darken-1 circle white-text hide-on-med-and-down">keyboard_arrow_right</i>
   			</div>
   		</a>
   		<div class="row purple lighten-1 center z-depth-1">
        	<?php showNewReviews(); ?>
   		</div>
   </div>
   <!-- Fixed control button -->
   <!-- FOOTER -->
   <?php 
   		showFixedBtn();
   		showFooter();
   ?>
    
</body>
</html>