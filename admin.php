<?php 
	session_start();
	if(!isset($_SESSION) or empty($_SESSION))
		header("Location: index.php");
	if(!isset($_SESSION['isAdmin']) or empty($_SESSION['isAdmin']))
		header("Location: index.php");
	include("phpScripts.php");	
	showFixedBtn();
	$mysqli = new mysqli('localhost', 'homestead', 'secret', 'TripOrTrick');

?>





<!DOCTYPE html>
<html>
<head>
	<title>Trip Or Trick: Administrator</title>
	    <!--Import Google Icon Font-->
	<link href='https://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/icon?family=Material+Icons' rel='stylesheet'>
    <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
    <!--Import materialize.css-->
    <link type='text/css' rel='stylesheet' href='css/materialize.min.css'  media='screen,projection'/>
    <link rel='shortcut icon' href='imgs/world.ico'>
    
    <!--Let browser know website is optimized for mobile-->
    <meta name='viewport' content='width=device-width, initial-scale=1.0'/>
    <link rel='stylesheet' type='text/css' href='css/style.css'>
    <div class='loader'></div>
</head>
<body class="blue-grey darken-2">
	<!--Import jQuery before materialize.js-->
    <script type='text/javascript' src='https://code.jquery.com/jquery-2.1.1.min.js'></script>
    <script type='text/javascript' src='js/materialize.min.js'></script>
	<script src='js/scripts.js'></script>
	<?php 
		if(isset($_GET['status']) and !empty($_GET['status']))
		checkToastStatus($_GET['status']);
	?>
		<div  style='padding:10px;'>
	  			<div class='row white-text' style='padding-left:20px;'>
	  				<i class='material-icons prefix left medium'>star</i>
	  				<h4 style='padding-top:12px;'>Administrator</h4>
	  			</div><hr>

	  			<div class="row white">
    				<div class="col s12">
      					<ul class="tabs">
        					<li class="tab col s3"><a href="#member_tab" class='active'>Members</a></li>
        					<li class="tab col s3"><a href="#review_tab">Reviews</a></li>
        					<li class="tab col s3"><a href="#comment_tab">Comments</a></li>
        					<li class="tab col s3"><a href="#request_tab">Requests</a></li>
      					</ul>
    				</div>
    				
  				</div>

	  			<div class='row' id='member_tab'>
	  				
	  				<div class="row white black-text">
	  				<h4>Members</h4>
	  					<table class="striped responsive-table">
	  						<thead>
		  						<tr>
	  								<th>ID</th>
	  								<th>Username</th>
	  								<th>Email</th>
	  								<th>No. of Reviews</th>
	  								<th>No. of Comments</th>
	  								<th>No. of Requests</th>
	  								<th></th>
	  							</tr>
		  					</thead>
	  						<tbody>
		  						<?php 	  								
	  								$query = "SELECT Members.ID, Members.isAdmin, Members.Username, Members.Email, IF(tempR.CountReview >= 1,tempR.CountReview,0) AS 'No. of Reviews', IF(tempC.CountComment >= 1,tempC.CountComment,0) AS 'No. of Comments', IF(tempQ.CountRequest >= 1,tempQ.CountRequest,0) AS 'No. of Requests' FROM Members LEFT JOIN (SELECT Reviews.Auther_ID, COUNT(Reviews.ID) AS CountReview FROM Reviews LEFT JOIN Members ON Reviews.Auther_ID = Members.ID GROUP BY Reviews.Auther_ID) AS tempR ON tempR.Auther_ID = Members.ID LEFT JOIN (SELECT Comments.Comment_auther, COUNT(Comments.Comment_ID) AS CountComment FROM Comments LEFT JOIN  Members ON Comments.Comment_auther = Members.ID GROUP BY Comments.Comment_auther) AS tempC ON tempC.Comment_auther = Members.ID LEFT JOIN (SELECT Requests.Member_ID, COUNT(Requests.Request_ID) AS CountRequest From Requests LEFT JOIN Members ON Requests.Member_ID = Members.ID GROUP BY Requests.Member_ID) AS tempQ ON tempQ.Member_ID = Members.ID ORDER BY Members.ID"; // HERE
		  							$result = $mysqli->query($query);
	  								while($row = $result->fetch_assoc()){
		  								echo "<tr>
	  												<td>{$row['ID']}</td>
	  												<td><a href='show.php?usr={$row['ID']}'>{$row['Username']}</a></td>
	  												<td>{$row['Email']}</td>
	  												<td>{$row['No. of Reviews']}</td>
	  												<td>{$row['No. of Comments']}</td>
	  												<td>{$row['No. of Requests']}</td>";
	  												if($row['Username'] == 'Deleted')
	  													echo "<td class='red-text'>Deleted<td>";
	  												else if($row['isAdmin'] == 0)
	  													echo "<td><a class='btn red' href='manage.php?s=11&t={$row['ID']}'>DELETE</a> <a class='btn green' href='manage.php?s=12&t={$row['ID']}'>SET ADMIN</a></td>";
	  												else if($row['ID'] == $_SESSION['id'])
	  													echo "<td></td>";
	  												else
	  													echo "<td><a class='btn orange' href='manage.php?s=13&t={$row['ID']}'>DROP ADMIN</a></td>";

		  									 echo "</tr>";
		  							}		  							
	  							?>
	  						</tbody>
	  					</table>	
	  				</div>
	  				
	  			</div>
	  			<div class='row' id='review_tab'>
	  				
	  				<div class="row white black-text">
	  					<h4>Reviews</h4>
	  					<table class="striped responsive-table centered">
	  						<thead>
		  						<tr>
	  								<th>ID</th>
	  								<th>Auther</th>
	  								<th>Topic</th>
	  								<th>No. of Comments</th>
	  								<th>Date</th>	  						  						
	  								<th></th>
	  							</tr>
		  					</thead>
	  						<tbody>
	  						<?php 
	  								$query = "SELECT Reviews.ID, Reviews.Auther_ID, Members.Username, Reviews.Review_topic,IF(temp.CountComment >= 1,temp.CountComment,0) AS 'No.Comments', DATE_FORMAT(Review_date,'%d %M %Y %h:%i %p') AS Review_date FROM Reviews LEFT JOIN (SELECT Comments.Review_ID, COUNT(Comments.Comment_ID) AS CountComment FROM Comments LEFT JOIN Reviews ON Comments.Review_ID = Reviews.ID GROUP BY Comments.Review_ID) AS temp ON temp.Review_ID = Reviews.ID LEFT JOIN Members ON Reviews.Auther_ID = Members.ID ORDER BY Reviews.ID"; // HERE
		  							$result = $mysqli->query($query);
	  								while($row = $result->fetch_assoc()){
		  								echo "<tr>
	  												<td>{$row['ID']}</td>
	  												<td><a href='show.php?usr={$row['Auther_ID']}'>{$row['Username']}</a></td>
	  												<td><a href='show.php?page_id={$row['ID']}'>{$row['Review_topic']}</a></td>
	  												<td>{$row['No.Comments']}</td>
	  												<td>{$row['Review_date']}</td>	  									
	  												<td><a class='btn red' href='manage.php?s=2&t={$row['ID']}'>DELETE</a></td>
		  									 </tr>";
		  							}		  						
	  						?>
	  						</tbody>
	  					</table>
	  				</div>
	  			</div>
	  			<div class='row' id='comment_tab'>	  				
	  				<div class="row white black-text">
	  					<h4>Comments</h4>
	  					<table class="striped responsive-table centered">
	  						<thead>
		  						<tr>
	  								<th>Comment ID</th>
	  								<th>Review ID</th>
	  								<th>Username</th>
	  								<th>Details</th>	  								
	  								<th>Date</th>	  						  						
	  								<th></th>
	  							</tr>
		  					</thead>
	  						<tbody>
	  						<?php 
	  								$query = "SELECT Members.ID, Comments.Comment_ID, Reviews.ID AS Review_ID, Members.Username, Comments.Comment_details, DATE_FORMAT(Comment_date,'%d %M %Y %h:%i %p') AS Comment_date FROM Comments LEFT JOIN Reviews ON Comments.Review_ID = Reviews.ID LEFT JOIN Members ON Reviews.Auther_ID = Members.ID ORDER BY Comments.Comment_ID;"; // HERE
		  							$result = $mysqli->query($query);
	  								while($row = $result->fetch_assoc()){
	  									$details = htmlentities($row['Comment_details']);
		  								echo "<tr>
	  												<td>{$row['Comment_ID']}</td>
	  												<td><a href='show.php?page_id={$row['Review_ID']}'>{$row['Review_ID']}</a></td>
	  												<td><a href='show.php?usr={$row['ID']}'>{$row['Username']}</a></td>	  					
	  												<td>{$details}</td>	  								
	  												<td>{$row['Comment_date']}</td>	  								
	  												<td><a class='btn red' href='manage.php?s=3&t={$row['Comment_ID']}'>DELETE</a></td>
		  									 </tr>";
		  							}		  					
	  						?>
	  						</tbody>
	  					</table>
	  				</div>
	  			</div>
	  			<div class='row' id='request_tab'>	  				
	  				<div class="row white black-text">
	  					<h4>Delete Member Request</h4>
	  					<table class="striped responsive-table">
	  						<thead class="">
		  						<tr>
	  								<th>Request ID</th>
	  								<th>Target Username</th>
	  								<th>Reporter</th>
	  								<th>Reason</th>	  								
	  								<th>Date</th>	  						  						
	  								<th></th>
	  							</tr>
		  					</thead>
	  						<tbody>
	  						<?php 
	  								$query = "SELECT m1.Username AS target, DEL_MEM_Request.Request_ID, DEL_MEM_Request.Member_ID AS 'Target Username', Members.Username AS 'Reporter', Members.ID AS 'Reporter ID',Requests.Reason,DATE_FORMAT(Requests.Request_Date,'%d %M %Y %h:%i %p') AS Request_Date, Requests.Request_Status, Requests.Admin_ID, DATE_FORMAT(Requests.Deleted_date,'%d %M %Y %h:%i %p') AS Deleted_Date, m2.Username FROM DEL_MEM_Request LEFT JOIN Requests ON DEL_MEM_Request.Request_ID = Requests.Request_ID LEFT JOIN Members AS m1 ON DEL_MEM_Request.Member_ID = m1.ID LEFT JOIN Members ON Members.ID = Requests.Member_ID LEFT JOIN Members AS m2 ON Requests.Admin_ID = m2.ID ORDER BY DEL_MEM_Request.Request_ID"; // HERE
		  							$result = $mysqli->query($query);
	  								while($row = $result->fetch_assoc()){	  									
		  								echo "<tr>	  												
	  											<th>{$row['Request_ID']}</th>
	  											<th><a href='show.php?usr={$row['Target Username']}'>{$row['target']}</a></th>
	  											<th><a href='show.php?usr={$row['Reporter ID']}'>{$row['Reporter']}</a></th>
	  											<th>{$row['Reason']}</th>	  								
		  										<th>{$row['Request_Date']}</th>";
		  										if($row['Request_Status'] == 0)
	  												echo ";<th><a class='btn red' href='manage.php?s=11&t={$row['Target Username']}'>DELETE</a></th>";
	  											else
	  												echo "<th class='red-text'>Deleted by <a href='show.php?usr={$row['Admin_ID']}'>{$row['Username']}</a> <br>on {$row['Deleted_Date']}</th>";

		  									 echo "</tr>";
		  							}		  							
	  						?>
	  						</tbody>
	  					</table>
	  					<hr><br>
	  					<h4>Delete Review Request</h4>
	  					<table class="striped responsive-table">
	  						<thead>
		  						<tr>
	  								<th>Request ID</th>
	  								<th>Target Review</th>
	  								<th>Reporter</th>
	  								<th>Reason</th>	  								
	  								<th>Date</th>	  						  						
	  								<th></th>
	  							</tr>
		  					</thead>
	  						<tbody>
	  						<?php 
	  								$query = "SELECT DEL_REVIEW_Request.Request_ID, DEL_REVIEW_Request.Review_ID AS 'Target Review', Members.Username AS 'Reporter', Members.ID AS 'Reporter ID',Requests.Reason,DATE_FORMAT(Requests.Request_Date,'%d %M %Y %h:%i %p') AS Request_Date,Requests.Request_Status, m2.Username,Requests.Admin_ID, DATE_FORMAT(Requests.Deleted_date,'%d %M %Y %h:%i %p') AS Deleted_Date FROM DEL_REVIEW_Request LEFT JOIN Requests ON DEL_REVIEW_Request.Request_ID = Requests.Request_ID LEFT JOIN Members ON Members.ID = Requests.Member_ID LEFT JOIN Members as m2 ON Requests.Admin_ID = m2.ID ORDER BY DEL_REVIEW_Request.Request_ID;"; // HERE
		  							$result = $mysqli->query($query);
	  								while($row = $result->fetch_assoc()){	  									
		  								echo "<tr>	  												
	  											<th>{$row['Request_ID']}</th>
	  											<th><a href='show.php?page_id={$row['Target Review']}'>{$row['Target Review']}</a></th>
	  											<th><a href='show.php?usr={$row['Reporter ID']}'>{$row['Reporter']}</th>
	  											<th>{$row['Reason']}</th>	  								
		  										<th>{$row['Request_Date']}</th>";
		  										if($row['Request_Status'] == 0)
	  												echo "<th><a class='btn red' href='manage.php?s=2&t={$row['Target Review']}'>DELETE</a></th>";
	  											else
	  												echo "<th class='red-text'>Deleted by <a href='show.php?usr={$row['Admin_ID']}'>{$row['Username']}</a> <br>on {$row['Deleted_Date']}</th>";

		  									 echo "</tr>";
		  							}		  							
	  						?>
	  						</tbody>
	  					</table>
	  					<hr><br>
	  					<h4>Delete Comment Request</h4>
	  					<table class="striped responsive-table">
	  						<thead>
		  						<tr>
	  								<th>Request ID</th>
	  								<th>Target Comment</th>
	  								<th>Reporter</th>
	  								<th>Reason</th>	  								
	  								<th>Date</th>	  						  						
	  								<th></th>
	  							</tr>
		  					</thead>
	  						<tbody>
	  						<?php 
	  								$query = "SELECT DEL_COMM_Request.Request_ID, DEL_COMM_Request.Comment_ID AS 'Target Comment', Members.Username AS 'Reporter', Members.ID AS 'Reporter ID',Requests.Reason,DATE_FORMAT(Requests.Request_Date,'%d %M %Y %h:%i %p') AS Request_Date, Requests.Request_Status, m2.Username,Requests.Admin_ID, DATE_FORMAT(Requests.Deleted_date,'%d %M %Y %h:%i %p') AS Deleted_Date FROM DEL_COMM_Request LEFT JOIN Requests ON DEL_COMM_Request.Request_ID = Requests.Request_ID LEFT JOIN Members ON Members.ID = Requests.Member_ID LEFT JOIN Members as m2 ON Requests.Admin_ID = m2.ID ORDER BY DEL_COMM_Request.Request_ID;"; // HERE
		  							$result = $mysqli->query($query);
	  								while($row = $result->fetch_assoc()){	  									
		  								echo "<tr>	  												
	  											<th>{$row['Request_ID']}</th>
	  											<th>{$row['Target Comment']}</th>
	  											<th><a href='show.php?usr={$row['Reporter ID']}'>{$row['Reporter']}</a></th>
	  											<th>{$row['Reason']}</th>	  								
		  										<th>{$row['Request_Date']}</th>";
		  										if($row['Request_Status'] == 0)
	  												echo "<th><a class='btn red' href='manage.php?s=3&t={$row['Target Comment']}'>DELETE</a></th>";
	  											else
	  												echo "<th class='red-text'>Deleted by <a href='show.php?usr={$row['Admin_ID']}'>{$row['Username']}</a> <br>on {$row['Deleted_Date']}</th>";
  						
		  									 echo "</tr>";
		  							}		  							
	  						?>
	  						</tbody>
	  					</table>
	  				</div>
	  			</div>	  			

	  			<h5 class="center"><a href='index.php' style="color: white;">Home</a></h5>	
	  		</div>
		</div>
</body>
</html>