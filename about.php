<!DOCTYPE html>
<html>
<head>
	<title>Trip Or Trick: Developers</title>
	<!--Import Google Icon Font-->
    <link href='https://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet' type='text/css'>
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
    <link rel="shortcut icon" href="imgs/world.ico">
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body class="grey lighten-4">
	<!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <script src="js/scripts.js"></script>
    
    <?php session_start(); 
        include('phpScripts.php');
    ?>
    <!-- Code Start -->
	<!-- Page Start -->
   <div class="my-container">
      <!-- Navbar -->
      <!-- Slider -->
      <?php
        showNav(); 
        showSlider();
      ?>

   	<!--  About -->
   	<!-- First Row -->
   		<div class="row blue-grey darken-4 center" style="margin-bottom:0px;">
   			  <i class="material-icons large" style="color:white;padding-top: 10px;">account_circle</i>
   			  <h3 class="white-text topic" style="margin:auto;padding-bottom: 10px;">DEVELOPERS</h3>
      </div>
   		<div class="row blue lighten-1 brown-text center z-depth-1">
          <div class="col s12 m6" style="padding-top: 20px;">
              <div class="row">
                  <div class="col s12">
                    <div class="card large">
                      <div class="card-image">
                        <img src="imgs/n.jpg">

                        <span class="card-title">Nunt Jensathienwong</span>
                      </div>
                      <div class="card-content left-align">
                        <p>Nunt Jensathienwong<br>570510660<br>@Computer Science Faculty of Science<br>Chiang Mai University.</p>
                      </div>
                      <div class="card-action">
                        <a href="https://www.facebook.com/Nunt504">Facebook</a>
                      </div>
                    </div>
                  </div>
                </div>
          </div>
          <div class="col s12 m6" style="padding-top: 20px;">
              <div class="row">
                  <div class="col s12">
                    <div class="card large">
                      <div class="card-image">
                        <img src="imgs/s.jpg" style="opacity: 1.0;">
                        <span class="card-title">Sirawich Toyai</span>
                      </div>
                      <div class="card-content left-align">
                        <p>Sirawich Toyai<br>570510689<br>@Computer Science Faculty of Science<br>Chiang Mai University.</p>
                      </div>
                      <div class="card-action">
                        <a href="https://www.facebook.com/kkeanez">Facebook</a>
                      </div>
                    </div>
                  </div>
                </div>
          </div>
   		</div>
    </div>

    <!-- Fixed control button -->
   <!-- FOOTER -->
   <?php 
      showFixedBtn();
      showFooter();
   ?>

</body>
</html>