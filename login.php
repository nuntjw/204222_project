<!DOCTYPE html>
<html>
<head>
    <title>Trip Or Trick: Login</title>
    <!--Import Google Icon Font-->
    <link href='https://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet' type='text/css'>
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
    <link rel="shortcut icon" href="imgs/world.ico">
    <!-- Bootstrap 
    <link href="css/bootstrap.min.css" rel="stylesheet">-->
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body class="login">
    <!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <link rel="shortcut icon" href="imgs/world.ico">
    <script src="js/scripts.js"></script>
    
    <!-- Bootstrap 
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>-->

    <div class="container white z-depth-4 form-input">
        <div class="white" style="margin-left:5%;margin-right:5%;">
            <h3><br><i class="material-icons medium left">account_box</i>Trip Or Trick</h3>
        </div>
        <div class="row center" style="margin-left:10px;margin-right:15px;">
            <div class="col l6 s12">
                <?php
                    session_start();
                    if(isset($_SESSION) and !empty($_SESSION))
                        header("Location: index.php");
                    include('phpScripts.php');
                    if(isset($_GET) and !empty($_GET))
                        if(isset($_GET['p']))
                            echo " <form name=\"loginForm\" method=\"post\" action=\"login_result.php?p={$_GET['p']}\">";
                        else{
                            if(isset($_GET['status']))
                                checkToastStatus($_GET['status']);
                            echo " <form name=\"loginForm\" method=\"post\" action=\"login_result.php\">";
                        }
                    else
                        echo " <form name=\"loginForm\" method=\"post\" action=\"login_result.php\">";
                 ?>
                    <div class="row" style="margin:0px;">
                        <div class="input-field col s12 m10">
                            <input id="username" name="username"  type="text" class="browser-default" required>
                            <label for="username">Username</label>
                        </div>
                    </div>
                    <div class="row" style="margin:0px;">
                        <div class="input-field col s12 m10">
                            <input id="password" type="password"  name="password" class="browser-default" required>
                            <label for="password">Password</label>
                        </div>
                    </div>
                    
                    <div class="row left" style="margin:0px;">
                        <button class="btn waves-effect waves-light green darken-2"style="margin:20px;" type="submit" name="btn" value="login">Login
                            <i class="material-icons right">check_circle</i>
                        </button>
                    </div>
                </form>
            </div>
            <div class="col l6 s12">
                <div class="card blue-grey darken-2 z-depth-2">
                    <div class="card-content white-text ">
                        <i class="material-icons" style="margin-top:10px;">warning</i>
                        <span class="card-title" style="font-weight:bold;">Warning</span><hr><br>
                        <div class="left-align"><p1>* Login with your username and password<br>* Carefully about your username and password are correct!<br>* Thank you for testing.</p1></div>                        
                    </div>
                </div>
            </div>
        </div><hr>
        <div class="row center"style="margin-left:10px;margin-right:10px;">
            <a href="register.php">Register</a> | <a href="index.php">Back to Home</a>
        </div>
    </div>
</body>
</html>