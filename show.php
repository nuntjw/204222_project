<!DOCTYPE html>
<html>
<head>
    <title>Trip Or Trick</title>
    <!--Import Google Icon Font-->
    <link href='https://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet' type='text/css'>
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
    <link rel="shortcut icon" href="imgs/world.ico">
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" type="text/css" href="css/style.css">

    <div class="loader"></div>
</head>
<body class="review-Bg">
    <!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <script src="js/scripts.js"></script>
    
    <?php session_start(); 
        include('phpScripts.php');
        if(!isset($_GET) or empty($_GET))
            header("Location: index.php");
    ?>
    <!-- Code Start -->
    <!-- Page Start -->
   <div class="my-container">
      <!-- Navbar --> 
      <?php
            showNav();
            if(isset($_GET['page_id'])){
                if(isset($_GET['status'])){
                    checkToastStatus($_GET['status']);
                }
                getReview($_GET['page_id']);
            }
            else if(isset($_GET['catalog']))
                getCatalog($_GET['catalog']);
            else if(isset($_GET['news'])){
                getNEWS($_GET['news']);
            }
            else if(isset($_GET['usr'])){
                if(isset($_GET['status'])){
                    checkToastStatus($_GET['status']);
                }
                showUser($_GET['usr']);
            }
            else{
                header("Location: index.php");
            }

            // end of my container
            echo "</div>";

            showFixedBtn();
            showFooter();
        ?>
</body>
</html>