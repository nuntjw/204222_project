$.getScript("materialize.js", function(){
    alert("Script loaded but not necessarily executed.");
});

var selector;
function sendReport(){
    var reportReason = $('#reportReason').val();
    var desID = $('#desID').html();
    //alert(desID);
    $.ajax({
      method: "POST",
      url: "send_report.php",
      data: {reason: reportReason, selector:selector, desID:desID}
    })
    .done(function(msg){
        //console.log(msg);
        Materialize.toast('sent report', 4000);
    });
}


$( document ).ready(function(){ 
    $(".button-collapse").sideNav();
    $('#al').click(function(){
        var addedTag = $('#wReview #content').val() + "\r\n[left][/left]";
        $('#wReview #content').val(addedTag);        
    });

    $('#ac').click(function(){
        var addedTag = $('#wReview #content').val() + "\r\n[cen][/cen]";
        $('#wReview #content').val(addedTag);
    });

    $('#ar').click(function(){
        var addedTag = $('#wReview #content').val() + "\r\n[right][/right]";
        $('#wReview #content').val(addedTag);
    });

    $('#li').click(function(){
        var addedTag = $('#wReview #content').val() + "\r\n[link][/link]";
        $('#wReview #content').val(addedTag);
    });

    $('#pic').click(function(){
        var addedTag = $('#wReview #content').val() + "\r\n[img][/img]";
        $('#wReview #content').val(addedTag);
    });

    $('#vid').click(function(){
        var addedTag = $('#wReview #content').val() + "\r\n[vid][/vid]";
        $('#wReview #content').val(addedTag);
    });

    $('#b').click(function(){
        var addedTag = $('#wReview #content').val() + "\r\n[b][/b]";
        $('#wReview #content').val(addedTag);
    });

    $('#underl').click(function(){
        var addedTag = $('#wReview #content').val() + "\r\n[ul][/ul]";
        $('#wReview #content').val(addedTag);
    });

    $('#ital').click(function(){
        var addedTag = $('#wReview #content').val() + "\r\n[ita][/ita]";
        $('#wReview #content').val(addedTag);
    });

    $('#quote').click(function(){
        var addedTag = $('#wReview #content').val() + "\r\n[quote][/quote]";
        $('#wReview #content').val(addedTag);
    });

    $('#nl').click(function(){
        var addedTag = $('#wReview #content').val() + "\r\n[br]";
        $('#wReview #content').val(addedTag);
    });

    $('.modal-trigger').click(function(){
        var id = $(this).data('id');
        if (typeof id !== 'undefined') {
            //alert(id);
            var type = Math.floor(id%10);
            //alert(type);
            id = Math.floor(id/10);
            $('#report #desID').html(id);
            if( type == 1){ // review
                $('#report #head').html("Review ID ");
                selector = 1;
            }else if(type == 2){ // comment
                $('#report #head').html("Comment ID ");
                selector = 2;
            }else{ // member
              selector = 3;
            }
        }
    });

    $('.dropdown-button').dropdown({
        inDuration: 300,
        outDuration: 225,
        constrain_width: true, // Does not change width of dropdown to that of the activator
        hover: true, // Activate on hover
        gutter: 0, // Spacing from edge
        belowOrigin: true, // Displays dropdown below the button
        alignment: 'left' // Displays dropdown with edge aligned to the left of button
    });  
    
    $('.datepicker').pickadate({
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 15 // Creates a dropdown of 15 years to control year
    });

    $('.tooltipped').tooltip({delay: 50});

    $('.slider').slider({full_width: true});
    
    $('.modal-trigger').leanModal();


    $('#editProfile').click(function(){
        //alert('edit profile');
        $.ajax({
            url: 'get_data.php',
            type: 'post',
            data: {
                id: $('#editID').html()
            },
            dataType: 'json',
            success: function(profile){
                /*for(col in profile){
                    alert(col);                
                }*/
                $('#editName').val(profile['Name']);
                $('#editSurname').val(profile['Surname']);
                $('#editMail').val(profile['Email']);
                $('#editDatepick').val(profile['Birthdate']);
                if(profile['sex'] == $('#sexM').val())
                    $('#sexM').prop('checked', true);
                else
                    $('#sexF').prop('checked', true);
            }
        });
    });
});


$(window).load(function() {
  $(".loader").fadeOut();
});

