<!DOCTYPE html>
<html>
<head>
    <title>TripOrTrick: Register</title>
    <!--Import Google Icon Font-->
    <link href='https://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet' type='text/css'>
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
    <link rel="shortcut icon" href="imgs/world.ico">
    <!-- Bootstrap 
    <link href="css/bootstrap.min.css" rel="stylesheet">-->
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body class="register">
    <!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <script src="js/scripts.js"></script>
    
    <!-- Bootstrap 
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>-->
    <?php
    session_start();
    if(isset($_GET['er']) and !empty($_GET['er'])) 
        if($_GET['er'] == 1)
            echo "<script>alert('this username is alreary exist in database!');</script>";
        else if ($_GET['er'] == 2) {
            echo "<script> alert('this username is not available!');</script>";
        }
    else if (isset($_SESSION) and !empty($_SESSION))
        header("Location: index.php");
    
    ?>
    <div class="container white z-depth-4 form-input">
        <div class="col s12 center"><h3><br>Trip Or Trick</h3></div>
        <form method="post" action="register_result.php" class="col s12">
            <div class="row" style="margin-right:8%;margin-left:5%;">
                <div class="input-field col s12 l6">
                    <i class="material-icons prefix">account_circle</i>
                    <input name="username" type="text" class="validate" length="20" required>
                    <label>Username</label>
                </div>
                <div class="input-field col s12 l6">
                    <i class="material-icons prefix">vpn_key</i>
                    <input name="password" type="password" class="validate" length="8" required>
                    <label>Password</label>
                </div>
            </div><br>
            <div class="row" style="margin-right:8%;margin-left:5%;">
                <div class="input-field col s12 l6">
                    <input name="name" type="text" class="validate" length="20" required>
                    <label>Name</label>
                </div>
                <div class="input-field col s12 l6">
                    <input name="surname" type="text" class="validate" length="20" required>
                    <label>Surname</label>
                </div>
                <div class="col s12 grey lighten-4">
                    <p class="grey-text" style="font-weight:bold;">Sex</p>
                    <p>
                        <input class="with-gap" name="gender" type="radio" id="sexM" value="Male" checked>
                        <label for="sexM">Male</label>
                        <input class="with-gap" name="gender" type="radio" id="sexF" value="Female">
                        <label for="sexF">Female</label>
                    </p><br>
                </div>
                <div class="input-field col s12 l6">
                    <i class="material-icons prefix">date_range</i>
                    <input type="date" name="birthDate" required>
                    
                </div>
                
                <div class="input-field col s12 l6">
                    <i class="material-icons prefix">contact_mail</i>
                    <input name="email" type="email" class="validate" length="40" required>
                    <label>Email</label>
                </div>
            </div>
            <div class="center">
                <input type="checkbox" class="filled-in" id="filled-in-box" name="checkRole" value="checked">
                <label for="filled-in-box">ยอมรับ<a class="modal-trigger" href="#modal1">ข้อตกลง</a><br></label>
            </div>
            
            <!-- Modal Structure -->
            <div id="modal1" class="modal modal-fixed-footer">
                <div class="modal-content">
                    <h4>ข้อตกลงการใช้งานเว็บไซต์</h4>
                    <p>1. สมาชิกต้องเคารพสิทธิของสมาชิกของอื่น<br>2. ผู้ดูแลสามารถลบผู้ใช้งานได้ตามเห็นสมควร<br>3. ห้ามนำรูปภาพจากเว็บไซต์อื่นหรือเนื้อหาจากเว็บไซต์อื่นมาใส่โดยไม่ได้รับอนุญาติ หรือไม่มีการให้เครดิต<br>4. ห้ามไม่ให้มีการซื้อขาย หรือการพนัน รวมถึงการโฆษณาภายในรีวิวหรือคอมเม้นต์<br>5. ขอสงวนสิทธิในการเปลี่ยนแปลงกฏโดยไม่ต้องถามสมาชิก หากมีกรณีจำเป็น</p>
                </div>
                <div class="modal-footer">
                <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">ปิด</a>
                </div>
            </div>
            <div class="center">
                <button class="btn waves-effect waves-light green darken-2" type="submit">สมัคร</button>
            </div>
            
        </form>
        <hr>
        <div class="row center"style="margin-left:10px;margin-right:10px;">
            <a href="index.php">Back to Home</a>
        </div>

    </div>
</body>
</html>