<?php
  function connect_db(){
      $mysqli = new mysqli('localhost', 'homestead', 'secret', 'TripOrTrick');
      return $mysqli;
  }

  function showSlider(){
    echo "<div class='slider z-depth-1 slider-wrapper white'>
          <ul class='slides'>
          <li>
            <img src='imgs/cover1.jpg'> <!-- random image -->
            <div class='caption center-align slider-content'>
              <h3>Mountain</h3>
              <h5 class='light grey-text text-lighten-3'>Fresh Air, Beautiful Views for your holidays</h5>
            </div>
          </li>
          <li>
            <img src='imgs/cover2.jpg'> <!-- random image -->
            <div class='caption left-align slider-content'>
              <h3>Night Life</h3>
              <h5 class='light grey-text text-lighten-3'>Night Club, Bars, Independent Living</h5>
            </div>
          </li>
          <li>
            <img src='imgs/cover3.jpg'> <!-- random image -->
            <div class='caption right-align slider-content'>
              <h3>Sea</h3>
              <h5 class='light grey-text text-lighten-3'>Sun Shine, Slow Life, Best For Your Holidays.</h5>
            </div>
          </li>
          <li>
            <img src='imgs/cover4.jpg'> <!-- random image -->
            <div class='caption center-align slider-content'>
              <h3>Food</h3>
              <h5 class='light grey-text text-lighten-3'>Food, Dessert, Drinks, Snack</h5>
            </div>
          </li>   
        </ul>
      </div>";
  }

  function showNav(){
    echo "<nav class='white'>
          <ul id='catalogs' class='dropdown-content'>
              <li><a href='show.php?catalog=1&&p=1'>Mountain<i class='material-icons right'>filter_hdr</i></a></li>
              <li><a href='show.php?catalog=2&&p=1'>Sea<i class='material-icons right'>pool</i></a></li>
              <li><a href='show.php?catalog=3&&p=1'>Night Life<i class='material-icons right'>location_city</i></a></li>
              <li><a href='show.php?catalog=4&&p=1'>Food<i class='material-icons right'>local_dining</i></a></li>
          </ul>
          <ul id='catalogs2' class='dropdown-content'>
              <li><a href='show.php?catalog=1&&p=1'>Mountain<i class='material-icons right'>filter_hdr</i></a></li>
              <li><a href='show.php?catalog=2&&p=1'>Sea<i class='material-icons right'>pool</i></a></li>
              <li><a href='show.php?catalog=3&&p=1'>Night Life<i class='material-icons right'>location_city</i></a></li>
              <li><a href='show.php?catalog=4&&p=1'>Food<i class='material-icons right'>local_dining</i></a></li>
          </ul>
         <div class='nav-wrapper'>
              <a href='index.php' class='brand-logo grey-text'>Trip Or Trick</a>
                  <a href='#' data-activates='mobile-demo' class='button-collapse grey-text'><i class='material-icons'>menu</i>
              </a>
              <ul class='right hide-on-med-and-down'>
                <li><a class='grey-text dropdown-button' data-activates='catalogs'>
                 <i class='material-icons right'>keyboard_arrow_down</i>CATEGORIES</a>
                </li>
                <li><a class='grey-text' href='about.php'><i class='material-icons right'>group</i>DEVELOPERS</a></li>";
    if(isset($_SESSION['user'])){
      if($_SESSION['isAdmin'] == 1){
            echo "<li><a class='red-text' href='show.php?usr={$_SESSION['id']}'>{$_SESSION['user']}<i class='material-icons right'>account_circle</i></a></li>
             <li><a class='grey-text' href='logout.php'>Log Out<i class='material-icons right'>exit_to_app</i></a></li>";
      }else{
          echo "<li><a class='blue-text' href='show.php?usr={$_SESSION['id']}'>{$_SESSION['user']}<i class='material-icons right'>account_circle</i></a></li>
            <li><a class='grey-text' href='logout.php'>Log Out<i class='material-icons right'>exit_to_app</i></a></li>";
      }
    }else{
      echo "<li><a class='grey-text' href='login.php'>LOGIN</a></li>
                <li><a class='grey-text' href='register.php'>REGISTER</a></li>";
    }
    echo "</ul>
              <ul class='side-nav' id='mobile-demo'>";

    if(isset($_SESSION['user'])){
        if($_SESSION['isAdmin'] == 1){
            echo "<li><a class='red-text' href='show.php?usr={$_SESSION['id']}'>{$_SESSION['user']}<i class='material-icons right'>account_circle</i></a></li>
             <li><a class='grey-text' href='logout.php'>Log Out<i class='material-icons right'>exit_to_app</i></a></li>";
        }else{
            echo "<li><a class='blue-text' href='show.php?usr={$_SESSION['id']}'>{$_SESSION['user']}<i class='material-icons right'>account_circle</i></a></li>
            <li><a class='grey-text' href='logout.php'>Log Out<i class='material-icons right'>exit_to_app</i></a></li>";
        }
    }else{
    echo "<li><a class='grey-text' href='login.php'>LOGIN</a></li>
                <li><a class='grey-text' href='register.php'>REGISTER</a></li>";
    }
    echo "<li><a class='grey-text dropdown-button' data-activates='catalogs2'>
                    <i class='material-icons right'>keyboard_arrow_down</i>CATEGORIES</a>
                </li>
                <li><a class='grey-text' href='about.php'><i class='material-icons right'>group</i>DEVELOPERS</a></li>
              </ul>
         </div>
      </nav>";
  }
  function showFixedBtn(){
    if(!isset($_SESSION['user'])){
      echo "<div class='fixed-action-btn' style='bottom: 45px; right: 24px;'>
    <a class='btn-floating btn-large red tooltipped' href='index.php' data-position='left'  data-delay='50' data-tooltip='กลับหน้าแรก'>
      <i class='large material-icons'>home</i>
    </a></div>";
    }else{
        echo "<div class='fixed-action-btn' style='bottom: 45px; right: 24px;'>
      <a class='btn-floating btn-large red darken-1'>
        <i class='large material-icons'>reorder</i>
      </a>
      <ul>
        <li><a href='show.php?usr={$_SESSION['id']}' class='btn-floating red tooltipped' data-position='left' data-delay='50' data-tooltip='ข้อมูลของคุณ'><i class='material-icons'>face</i></a></li>
        <li><a href='write.php' class='btn-floating yellow tooltipped' data-position='left' data-delay='50' data-tooltip='เขียนรีวิว'><i class='material-icons'>create</i></a></li>
        <li><a href='index.php' class='btn-floating green tooltipped' data-position='left' data-delay='50' data-tooltip='กลับหน้าแรก'><i class='material-icons'>home</i></a></li>
        <li><a href='logout.php' class='btn-floating blue tooltipped' data-position='left' data-delay='50' data-tooltip='ออกจากระบบ'><i class='material-icons'>exit_to_app</i></a></li>";

        if($_SESSION['isAdmin'] == 1){
          echo "<li><a href='admin.php' class='btn-floating black tooltipped' data-position='left' data-delay='50' data-tooltip='ผู้ดูแล'><i class='material-icons'>build</i></a></li>";
        }
        echo "</ul></div>";
    }
  }

  function showFooter(){
    echo "<footer class='page-footer grey darken-4'>
        <div class='container'>
            <div class='row center white-text'>
                <div class='col s6 left-align'>
                    Categories<br>
                    <a href='show.php?catalog=1&&p=1'>Mountain</a><br>
                    <a href='show.php?catalog=2&&p=1'>Sea</a><br>
                    <a href='show.php?catalog=3&&p=1'>Night Life</a><br>
                    <a href='show.php?catalog=4&&p=1'>Food</a>
                </div>
                <div class='col s6 left-align'>
                    About Us<br>
                    <a href='#'>FACEBOOK</a><br>
                    <a href='#'>LINE</a><br>
                    <a href='#'>TWITER</a><br>
                    <a href='#'>INSTAGRAM</a><br>
                </div>        
            </div>
        </div>
        <div class='footer-copyright'>
            <div class='container center'>
                Designed by NuntJW
            </div>
        </div>
    </footer>";
  }
  function unTags($textInput){
    //echo htmlentities($textInput);
    /* TAG Youtube's video */
    // style 1
    $text = str_replace("<div class=\"container\"><div class=\"video-container\">
          <iframe width=\"200\" height=\"100\" src=\"//www.youtube.com/embed/","[vid]https://www.youtube.com/watch?v=", $textInput);
    // style 2
    $text = str_replace("<div class=\"container\"><div class=\"video-container\">
          <iframe width=\"200\" height=\"100\" src=\"//www.youtube.com/embed/", "[vid]https://youtu.be/",$text);

    //trailer
    $text = str_replace("?rel=0\" frameborder=\"0\" allowfullscreen></iframe>
      </div></div>", "[/vid]", $text);

      /* TAG IMAGE */
    $text = str_replace("<img class=\"responsive-img\" src = \"","[img]",$text);
    $text = str_replace("\">","[/img]", $text);

    /* TAG Left align */
    $text = str_replace("<div class=\"left-align\">","[left]",$text);
    $text = str_replace("</div>","[/left]", $text);
    /* TAG Center align */
    $text = str_replace("<div class = \"center-align\">","[cen]",$text);
    $text = str_replace("</div>","[/cen]", $text); 
    /* TAG Right align */
    $text = str_replace("<div class = \"right-align\">","[right]",$text);
    $text = str_replace("[/div]","</right>", $text);
    /* TAG Link */
    $text = str_replace("<a href=\"","[link]",$text);
    $text = str_replace("\">Link</a>","[/link]", $text);
    /* TAG Color */
    /* TAG Bold */
    $text = str_replace("<b>","[b]",$text);
    $text = str_replace("</b>","[/b]", $text);
    /* TAG Underline */
    $text = str_replace("<u>","[ul]",$text);
    $text = str_replace("</u>","[/ul]", $text);
    /* TAG Newline */
    $text = str_replace("<br>","[br]",$text);
    /* TAG Italic */
    $text = str_replace("<i>","[ita]",$text);
    $text = str_replace("</i>","[/ita>", $text);
    /* TAG Quate */
    $text = str_replace("<blockquote><h5>\" ", "[quote]", $text);
    $text = str_replace(" \"</h5></blockquote>", "[/quote]", $text);
    /* TAG Emotion */


  
    //echo htmlentities($text);
    return $text;
  }

  function addTags($textInput){
    //echo nl2br($_POST['content']).'<br>'; // original text
    $textInput = strip_tags($textInput);
    $text = str_replace("[img]","<img class=\"responsive-img\" src = \"",nl2br($textInput));
    $text = str_replace("[/img]","\">", $text);
    //echo htmlentities($text);

    /* TAG Left align */
    $text = str_replace("[left]","<div class=\"left-align\">",$text);
    $text = str_replace("[/left]","</div>", $text);
    /* TAG Center align */
    $text = str_replace("[cen]","<div class = \"center-align\">",$text);
    $text = str_replace("[/cen]","</div>", $text); 
    /* TAG Right align */
    $text = str_replace("[right]","<div class = \"right-align\">",$text);
    $text = str_replace("[/right]","</div>", $text);
    /* TAG Link */
    $text = str_replace("[link]","<a href=\"",$text);
    $text = str_replace("[/link]","\">Link</a>", $text);
    /* TAG Color */
    /* TAG Bold */
    $text = str_replace("[b]","<b>",$text);
    $text = str_replace("[/b]","</b>", $text);
    /* TAG Underline */
    $text = str_replace("[ul]","<u>",$text);
    $text = str_replace("[/ul]","</u>", $text);
    /* TAG Newline */
    $text = str_replace("[br]","<br>",$text);
    /* TAG Italic */
    $text = str_replace("[ita]","<i>",$text);
    $text = str_replace("[/ita]","</i>", $text);
    /* TAG Quate */
    $text = str_replace("[quote]", "<blockquote><h5>\" ", $text);
    $text = str_replace("[/quote]", " \"</h5></blockquote>", $text);
    /* TAG Emotion */


    /* TAG Youtube's video */
    // style 1
    $text = str_replace("[vid]https://www.youtube.com/watch?v=", "<div class=\"container\"><div class=\"video-container\">
          <iframe width=\"200\" height=\"100\" src=\"//www.youtube.com/embed/", $text);
    // style 2
    $text = str_replace("[vid]https://youtu.be/","<div class=\"container\"><div class=\"video-container\">
          <iframe width=\"200\" height=\"100\" src=\"//www.youtube.com/embed/",$text);

    //trailer
    $text = str_replace("[/vid]", "?rel=0\" frameborder=\"0\" allowfullscreen></iframe>
      </div></div>", $text);
    //echo htmlentities($text);
    return $text;
  }

  function showComment($comment, $no){
      if($no % 2 == 0){
          $color = "row comment-row white index-show-topic";     
      }else if($no == 1){
          $color = "row comment-row grey lighten-2";
      }else{
          $color = "row comment-row grey lighten-2 index-show-topic";
      }

      echo "
        <div class='{$color}'>
            <div class='row' style='padding-top:10px'>
                <div class='col s6 left-align' style='padding-left:25px;'>ความคิดเห็นที่ <b>#{$no}</b></div>
                <div class='col s6 right-align'>";
      if(isset($_SESSION['user'])){
        echo "<a class='modal-trigger'  data-id='{$comment['Comment_ID']}2' href='#report'>แจ้งลบ</a>";
      }
      echo "    </div>
            </div>
            <div class='col s12 l9 push-l3'>
                <div class='row'>
                    <div class='col s12 auto-newline'><p>{$comment['Comment_details']}</p></div>
                </div>
            </div>
            <div class='col s12 l3 left pull-l9'>
                <div class='row'>
                    <div class='col s3 m2 l12'>
                        <div class='valign-wrapper'>";
            echo "          <img src=\"{$comment['Picture']}\" alt='' class='circle responsive-img valign' style='max-width:100px;'>
                          </div>";
        echo "        </div>
                      <div class='col s9 m10 l12' style='padding-top:10px;'>
                          <div class='row'>
                                <div class='col s12 m12 auto-newline'><b>Name</b> ";
                                if($comment['Username'] == 'Deleted')
                                  echo "<span>{$comment['Username']}</span>";
                                else
                                  echo "<a href='show.php?usr={$comment['ID']}'>{$comment['Username']}</a>";

        echo "                  </div>
                                <div class='col s12 m12 auto-newline'><b>Email</b> {$comment['Email']}</div>
                                <div class='col s12 m12 s12'><b>DATE</b> {$comment['timedate']}</div>
                          </div>
                      </div>
          
                  </div>
              </div>
         
        </div>";
  }

  function showFormComment($id){
      echo "<div class='row grey lighten-4 comment hoverable '>
                <hr><div class='col s12 white'>
                <h5><i class='material-icons'>chat_bubble</i> Write Comment</h5>
            </div>";
      if(isset($_SESSION['user'])){
      echo "<form action='sent_comment.php?id={$id}' method='post'>
                 <div class='row grey lighten-4'>
                 <div class='col s12'>
                    <i class='material-icons prefix'>account_circle</i>
                    <span>Username <b class='black-text'>{$_SESSION['user']}</b></span>
                    <i class='material-icons prefix'>mail</i>
                    <span>Email <b class='black-text'>{$_SESSION['mail']}</b></span>
                </div>
                <div class='col s12 grey lighten-4'>
                    <div class='input-field col s12'>
                        <textarea id='textarea1' class='white' name='commentBody' placeholder='write comment here' style='width:50%;height:100px;'></textarea>
                    </div>
                </div>
               
              </div>
              <div class='row grey lighten-4'><div class='col s12' style='margin-left:1%;'>
                    <button class='waves-effect waves-light btn-large' type='submit'><i class='material-icons left'>cloud</i> Send</button>
                </div></div>
            </form>
        </div>
            ";
      }else{
        echo "<div class ='row'>
            <div class='col s12 grey-text'>กรุณา<a href='login.php?p={$id}'>เข้าสู่ระบบ</a>เพื่อเขียนคอมเมนต์</div>
        </div></div>";
      }
}


  function showNewReviews(){
      $i = 0;
      $mysqli = new mysqli('localhost', 'homestead', 'secret', 'TripOrTrick');
      $query = "SELECT Reviews.ID, Members.Username, Reviews.Review_topic, Reviews.Review_content, Reviews.Review_picture, IF(temp.CountComment >= 1,temp.CountComment,0) as newCount FROM Reviews LEFT JOIN Members ON Reviews.Auther_ID = Members.ID LEFT JOIN ( SELECT Comments.Review_ID, COUNT(Comments.Comment_ID) AS CountComment FROM Comments LEFT JOIN Reviews ON Comments.Review_ID = Reviews.ID GROUP BY Comments.Review_ID )temp ON temp.Review_ID = Reviews.ID WHERE Reviews.ID > ( (select COUNT(Reviews.ID) from Reviews) - 4) ORDER BY Reviews.ID";
      if($result = $mysqli->query($query)){
          while ($newReview = $result->fetch_assoc()) {
              $newReviewArr[$i] = $newReview; // storing
              $i++; 
            }
              // reverse show
              // big first
              echo "<div class='col s12 l6 review'>
            <a href='show.php?page_id={$newReviewArr['4']['ID']}' class='white-text'>
              <div class='topic-img center' style='background: linear-gradient( rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4) ), url(\"{$newReviewArr['4']['Review_picture']}\") center;background-size: cover;'>
                 <div class='row topic-title3'>
                      <div class='row' style='margin-bottom:5px;'>
                        <h5 class='zero-m-bottom'>{$newReviewArr['4']['Review_topic']}</h5>
                      </div>
                      <div class='row right-align'>
                          <div class'col s12' style='padding-right:40px;'>
                            by {$newReviewArr['4']['Username']}
                            <i class='material-icons'>comment</i><span>{$newReviewArr['4']['newCount']}</span>
                          </div>
                      </div>
                  </div>
              </div>
            </a>
          </div>";

          // small
          $count = 1;
          echo "<div class='col s12 l6 review' style='padding-right:0px;padding-left:0px;'>";
          for($i = 3; $i >= 0; $i--){
              if($i == 3 or $i == 1){
                  echo "<div class='row zero-m-bottom'> ";
              }
              echo "<div class='col s12 m6 review new-small'>
                  <a href='show.php?page_id={$newReviewArr[$i]['ID']}' class='white-text'>
                    <div class='topic-img center topic-bg-2' style='background: linear-gradient( rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4) ), url(\"{$newReviewArr[$i]['Review_picture']}\") center;background-size: cover;'>
                        <div class='row topic-title2'>
                            <div class='row zero-m-bottom'>
                                <h5 class='zero-m-bottom'>{$newReviewArr[$i]['Review_topic']}</h5>
                            </div>
                            <div class='row right-align'>
                                <div class'col s12' style='padding-right:40px;'>
                                    by {$newReviewArr[$i]['Username']}
                                    <i class='material-icons'>comment</i><span>{$newReviewArr[$i]['newCount']}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                  </a>
                </div>";
              if($i == 2 or $i == 0){
                echo "</div>";
              }
          }
          echo "</div>";
          
      }else{
        $mysqli->close();
        exit("error while fetch news reviews");
      }

      $mysqli->close();
  }

  function showHotReviews(){
      $count = 1;
      $mysqli = new mysqli('localhost', 'homestead', 'secret', 'TripOrTrick');
      $query = "SELECT COUNT(Comments.Review_ID) AS newCount ,Reviews.ID AS revID, Members.ID AS memID, Members.Username, Reviews.Review_topic, Reviews.Review_content, Reviews.Review_picture FROM Reviews LEFT JOIN Comments ON Reviews.ID = Comments.Review_ID LEFT JOIN Members ON Reviews.Auther_ID = Members.ID GROUP by Reviews.ID ORDER BY newCount DESC LIMIT 4";
      if($result = $mysqli->query($query)){
          while($hotReview = $result->fetch_assoc()){
              if($count == 1){
                  echo "<div class='col s12 '>
            <a href='show.php?page_id={$hotReview['revID']}' class='white-text'>
              <div class='topic-img center zero-m-bottom' style='background: linear-gradient( rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4) ), url(\"{$hotReview['Review_picture']}\") center;background-size: cover;margin-bottom:10px;'>
                  <div class='row topic-title1'>
                      <div class='row' style='margin-bottom:5px;'>
                          <h5 style='margin-top:40px;'>{$hotReview['Review_topic']}</h5>
                      </div>
                      <div class='row right-align'><div class'col s12' style='padding-right:40px;'>
                          by {$hotReview['Username']}
                          <i class='material-icons'>comment</i><span>{$hotReview['newCount']}</span>
                      </div>
                      </div>
                  </div>
                  
           
              </div>
            </a>
        </div>";
              }else{
                  echo "<div class='col s12 l4 review'>
          <a href='show.php?page_id={$hotReview['revID']}' class='white-text'>
            <div class='topic-img center topic-bg-1' style='background: linear-gradient( rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4) ), url(\"{$hotReview['Review_picture']}\") center;background-size: cover;'>
                <div class='row topic-title2'>
                      <div class='row' style='margin-bottom:5px;'><h5 style='margin-top:40px;'>{$hotReview['Review_topic']}</h5></div>
                      <div class='row right-align'><div class'col s12' style='padding-right:40px;'>
                            by {$hotReview['Username']}
                            <i class='material-icons'>comment</i><span>{$hotReview['newCount']}</span>
                      </div>
                      </div>
                  </div>
            </div></a>
        </div>";
              }
              $count ++;
          }
      }else{
          $mysqli->close();
          echo "Failed Query!";
      }
      $mysqli->close();

  }
  function uploadImage($src){
    $target_dir = "imgs/";
    $target_file = $target_dir . basename($_FILES[$src]["name"]);
    $uploadOk = 1;
    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
    // Check if image file is a actual image or fake image
    if(isset($_POST["submitBtn"]) == 'send') {
        $check = getimagesize($_FILES[$src]["tmp_name"]);
        if($check !== false) {
            echo "File is an image - " . $check["mime"] . ".";
            $uploadOk = 1;
        } else {
            echo "File is not an image.";
            $uploadOk = 0;
        }
    }
    // Check if file already exists
    if (file_exists($target_file)) {
        echo "Sorry, file already exists.";
        $uploadOk = 0;
    }
    // Check file size
    if ($_FILES[$src]["size"] > 1000000) {
        echo "Sorry, your file is too large.";
        $uploadOk = 0;
    }
    // Allow certain file formats
    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg") {
        echo "Sorry, only JPG, JPEG, PNG files are allowed.";
        $uploadOk = 0;
    }
    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
        echo "Sorry, your file was not uploaded.";
        return false;
    // if everything is ok, try to upload file
    } else {
        if (move_uploaded_file($_FILES[$src]["tmp_name"], $target_file)) {
            echo "The file ". basename($_FILES[$src]["name"]). " has been uploaded.";
            return $target_file;
        } else {
            echo "Sorry, there was an error uploading your file.";
            return false;
        }   
    }
  }

  function showTopic($topic){
      echo "<div class='col s12 offset-s1'><span class='red-text'><b>Topic</b></span></div>";
  }

  function findCatalog($id){
      if($id == 1)
          return "Mountain";
      else if($id == 2){
          return "Sea";
      }else if($id == 3){
          return "Night Life";
      }else{
          return "Food";
      }
  }


  function showReview($review){
      $catalog = findCatalog($review['Category']);
      echo "<div class='row grey lighten-4 index-show-topic'>
              <div class='col s12'>
                  <div class='center review-cover-img' style='background:url(\"{$review['Review_picture']}\") no-repeat center center;'>
                  </div>
              </div>
            </div>
            <div class='row right zero-m-bottom'style='margin-right:10px;'>
              <div class='col s12'>";
      
      if(isset($_SESSION['user'])){
          if($_SESSION['isAdmin'] == 1 or $_SESSION['user'] == $review['Username']){
              echo "<a href='write.php?p={$review['ID']}'>แก้ไข</a>";
                    
          }
      }          
        
      echo "   </div>
              </div>
              <div class='row white review-row'>
                  <div class='col s12'><h5 class='red-text'><b>{$review['Review_topic']}</b></h5></div>
                  <div class='col s12 grey-text'>
                    <i class='material-icons prefix' >date_range</i>
                    <b>เขียนเมื่อ {$review['timedate']}</b>
                    <i class='material-icons prefix' >pin_drop</i>
                    <b>หมวดหมู่ <a href='show.php?catalog={$review['Category']}'>{$catalog}</a></b>
                  </div>
                  <div class='col s12 left-align' style='margin-bottom:3%;'><hr>
                    <div class='container'>
                    <div class='row'>
                        {$review['Review_content']}
                    </div>
                    </div>
                  </div>
                  <div class='col s12 right-align red-text'>{$review['edit']}</div>
                  <div class='col s12 right-align'>";
      if(isset($_SESSION['user'])){
            echo "      <a  class='modal-trigger' id='rep' data-id='{$review['ID']}1' href='#report'>รีวิวนี้ไม่เหมาะสม?</a>";
                  }
                  echo "</div><br>
                  <div class='col s12 grey lighten-4'><hr>
                      <div class='row right-align zero-m-bottom'>
                          <div class='col s4 m8 left-align red-text'>
                              <div class='row'>
                                  <a href='https://www.facebook.com/' ><img data-position='right' data-delay='50' data-tooltip='แชร์ริวิวนี้' class='responsive-img tooltipped' style='width:75px;' src='imgs/logo-facebook.png'></a>
                              </div>
                          </div>                            
                          <div class='col s8 m4'>
                              <div class='row zero-m-bottom'>
                                  <div class='col s5 m5 l3'><img class='responsive-img circle z-depth-1' src=\"{$review['Picture']}\"></div>
                                  <div class='col s7 m7 l9 left-align' style='margin-top:15px;'>หมายเลขรีวิว <b class='brown-text'>{$review['ID']}</b><br>เขียนโดย <a href='show.php?usr={$review['Auther_ID']}'><b class='brown-text'>{$review['Username']}</b></a></div>
                              </div>
                          </div>
                      </div>
                  </div>
                  ";
        echo "</div>";  

  }

  function getReview($reviewNo){
       $mysqli = new mysqli('localhost', 'homestead', 'secret', 'TripOrTrick');
       $query = "SELECT Members.Username, Members.Picture, Reviews.ID, Reviews.Auther_ID, Reviews.Category, Reviews.Review_topic, Reviews.Review_content, Reviews.Review_picture, Reviews.edit, DATE_FORMAT(Review_date,'%d %M %Y %h:%i %p') AS timedate FROM Members JOIN Reviews ON Reviews.Auther_ID = Members.ID WHERE Reviews.ID = {$reviewNo}";
       //$query = "SELECT *FROM Reviews WHERE ID = {$reviewNo}";
       if($result = $mysqli->query($query)){
          $data = $result->fetch_assoc();
          showReview($data);
          echo "<script> document.title = \"Trip Or Trick: {$data['Review_topic']}\"</script>";
          // Comments
          $query = "SELECT Comments.Comment_ID, DATE_FORMAT(Comments.Comment_date,'%d %M %Y %h:%i %p') AS timedate, Members.Username, Comments.Comment_details, Members.Email, Members.Picture, Members.ID FROM Members LEFT JOIN Comments ON Comments.Comment_auther = Members.ID WHERE Review_ID = {$reviewNo} ORDER BY Comment_ID";
          if($result = $mysqli->query($query)){
              echo "<div class='row white comment'><div class='col s12'><h5><i class='material-icons'>description</i>Comments</h5></div></div>";
              $count = 1;              
              while ($comment = $result->fetch_assoc()) {
                  showComment($comment, $count);
                  $count++;
              }
              if($result->num_rows == 0){
                  echo "<div class='row grey lighten-4 grey-text review-row'><div class='col s12' style='padding-bottom:10px;'>ไม่มีคอมเมนต์</div></div>";
              }
              // show Form after showed comment
              showFormComment($reviewNo);

              // แจ้งลบ modal
              echo "<div id='report' class='modal modal-fixed-footer'>
                <div class='modal-content'>
                    <div class='row'><i class='material-icons prefix medium left'>edit</i><b>แจ้งลบ <h4 class='red-text'><span id='head'></span><span id='desID'></span></h4></b></div>
                    
                    <h5>เหตุผล</h5>
                    <textarea id='reportReason' name='commentBody'></textarea>
                </div>
                <div class='modal-footer'>
                <a id='reportBtn' onclick='sendReport();' class='modal-action modal-close waves-effect waves-green btn-flat'><i class='red-text material-icons prefix left'>send</i>ส่ง</a>
                </div>
            </div>";
          }else{
            $mysqli->close;
              exit('Fialed comments');
          }
          
       }else{
          $mysqli->close();
          header("Location: index.php");
       }
       $mysqli->close();
  }

  function categoryText($text){
      $text = strip_tags($text);
      return substr($text, 0, 600)."...";
  }

  function showNewReviewsAll(){
      $mysqli = new mysqli('localhost', 'homestead', 'secret', 'TripOrTrick');
      $offset = 6 * ($_GET['p'] - 1);
      $query = "SELECT * FROM (SELECT Reviews.ID,Members.ID AS memID,DATE_FORMAT(Reviews.Review_date,'%d %M %Y') AS timedate , Members.Username, Reviews.Review_topic,Reviews.ID AS revID , Reviews.Review_content, Reviews.Review_picture, IF(temp.CountComment >= 1,temp.CountComment,0) AS newCount FROM Reviews LEFT JOIN Members ON Reviews.Auther_ID = Members.ID LEFT JOIN ( SELECT Comments.Review_ID, COUNT(Comments.Comment_ID) AS CountComment FROM Comments LEFT JOIN Reviews ON Comments.Review_ID = Reviews.ID GROUP BY Comments.Review_ID )temp ON temp.Review_ID = Reviews.ID ) AS reverse ORDER BY reverse.ID DESC LIMIT 6 OFFSET {$offset}";
      if($result = $mysqli->query($query)){
          while($review = $result->fetch_assoc()){
              $content = categoryText($review['Review_content']);
              echo "<div class='row' style='padding-top:5px;'>
                        <div class='col s12 m3'><a href='show.php?page_id={$review['revID']}'><img class='responsive-img' src=\"{$review['Review_picture']}\"></a></div>
                        <div class='col s12 m9 left-align'>
                            <a href='show.php?page_id={$review['revID']}'>
                                <div class='row zero-m-bottom'><h5>{$review['Review_topic']}</h5></div>
                                <div class='row left-align'><p class='auto-newline'>{$content}</p></div>
                            </a>
                            <div class='row right-align zero-m-bottom'>                              
                                <span><i class='material-icons prefix'>account_circle</i><a href='show.php?usr={$review['memID']}'>{$review['Username']}</a></span>
                                <span><i class='material-icons prefix'>comment</i>{$review['newCount']} comments</span>
                                <span><i class='material-icons prefix'>date_range</i>{$review['timedate']}</span>
                            </div>                          
                        </div>
                    </div><hr>";
          }
      }else{
          echo "Failed SQL command";
      }
      $mysqli->close();
  }

  function showHotReviewsAll(){
      $mysqli = new mysqli('localhost', 'homestead', 'secret', 'TripOrTrick');
      $query = "SELECT COUNT(Comments.Review_ID) AS newCount ,Reviews.ID AS revID, DATE_FORMAT(Reviews.Review_date,'%d %M %Y') AS timedate, Members.ID AS memID, Members.Username, Reviews.Review_topic, Reviews.Review_content, Reviews.Review_picture FROM Reviews LEFT JOIN Comments ON Reviews.ID = Comments.Review_ID LEFT JOIN Members ON Reviews.Auther_ID = Members.ID GROUP by Reviews.ID ORDER BY newCount DESC LIMIT 6";
      if($result = $mysqli->query($query)){
          while($review = $result->fetch_assoc()){
              $content = categoryText($review['Review_content']);
              echo "<div class='row' style='padding-top:5px;'>
                        <div class='col s12 m3'><a href='show.php?page_id={$review['revID']}'><img class='responsive-img' src=\"{$review['Review_picture']}\"></a></div>
                        <div class='col s12 m9 left-align'>
                            <a href='show.php?page_id={$review['revID']}'>
                                <div class='row zero-m-bottom'><h5>{$review['Review_topic']}</h5></div>
                                <div class='row left-align'><p class='auto-newline'>{$content}</p></div>
                            </a>
                            <div class='row right-align zero-m-bottom'>                              
                                <span><i class='material-icons prefix'>account_circle</i><a href='show.php?usr={$review['memID']}'>{$review['Username']}</a></span>
                                <span><i class='material-icons prefix'>comment</i>{$review['newCount']} comments</span>
                                <span><i class='material-icons prefix'>date_range</i>{$review['timedate']}</span>
                            </div>                          
                        </div>
                    </div><hr>";
          }
      }else{
          echo "Failed SQL command";
      }
      $mysqli->close();
  }

  function getNEWS($selector){
      showSlider();

      echo "<div class='row white'>
                <div class='col s12' style='padding-top:5px;'>
                    ";
      if($selector == "news"){
          $mysqli = new mysqli('localhost', 'homestead', 'secret', 'TripOrTrick');
          $query = "SELECT COUNT(*) AS count FROM Reviews";
          $result = $mysqli->query($query);
          $count = $result->fetch_assoc();
          $pageNo = ceil($count['count'] / 6);
          echo "<script> document.title = \"Trip Or Trick: New Reviews\"</script>";
        
          echo "<i class='material-icons medium left'>assistant</i><h4>News Reviews</h4>
                </div>
                <div class='col s12'><hr>
                <div class='my-container'>";
          showNewReviewsAll();
          echo "</div>
            </div>";
          showPageinationNew($pageNo, $_GET['p']);
          echo "</div></div>";
                  
      }else if($selector == "hots"){
        echo "<script> document.title = \"Trip Or Trick: Hot Reviews\"</script>";
        
      echo "<i class='material-icons medium left'>assistant</i><h4>Hot Reviews</h4>
                </div>
                <div class='col s12'><hr>
                <div class='my-container'>";
      showHotReviewsAll();                
      echo "</div>
            </div>
            </div></div>";
      }else{
        echo "<script>window.location.href = 'index.php';</script>";
      }
      
  }

  function showPageinationNew($pageNo, $p){
    if($p > $pageNo or $p < 1)
      echo "<script>window.location.replace('index.php');</script>";
    
      echo "
            <div class='row'>
                <div class='col s12 center'>
                    <ul class='pagination'>";
                if($pageNo == 1){
                    echo "<li class='disabled'><a><i class='material-icons'>chevron_left</i></a></li>
                          <li class='active'><a>1</a></li>
                          <li class='disabled'><a><i class='material-icons'>chevron_right</i></a></li>
                    ";
                }else{
                    $previous = $p - 1;
                    if($p == 1){
                        echo "<li class='disabled'><a><i class='material-icons'>chevron_left</i></a></li>";
                    }else{
                        echo "<li><a href='show.php?news=news&&p={$previous}'><i class='material-icons'>chevron_left</i></a></li>";
                    }               
                    
                    for($i = 1; $i <= $pageNo; $i++){
                        if($i == $p){
                            echo "<li class='active'><a>{$i}</a></li>";
                        }else{
                          echo "<li><a href='show.php?news=news&&p={$i}'>{$i}</a></li>";
                        }
                    }
                    $next = $p + 1;                    
                    if(($next - 1) == $pageNo){
                        echo "<li class='disabled'><a><i class='material-icons'>chevron_right</i></a></li>";
                    }else{
                        echo "<li class='waves-effect'><a href='show.php?news=news&&p={$next}'><i class='material-icons'>chevron_right</i></a></li>";
                    }
                    
                }

              echo "</ul>
                </div>";
  }

  function showPageination($pageNo, $p, $catalogID){
      echo "
            <div class='row'>
                <div class='col s12 center'>
                    <ul class='pagination'>";
                if($pageNo == 1){
                    echo "<li class='disabled'><a><i class='material-icons'>chevron_left</i></a></li>
                          <li class='active'><a>1</a></li>
                          <li class='disabled'><a><i class='material-icons'>chevron_right</i></a></li>
                    ";
                }else{
                    $previous = $p - 1;
                    if($p == 1){
                        echo "<li class='disabled'><a><i class='material-icons'>chevron_left</i></a></li>";
                    }else{
                        echo "<li><a href='show.php?catalog={$catalogID}&&p={$previous}'><i class='material-icons'>chevron_left</i></a></li>";
                    }               
                    
                    for($i = 1; $i <= $pageNo; $i++){
                        if($i == $p){
                            echo "<li class='active'><a>{$i}</a></li>";
                        }else{
                          echo "<li><a href='show.php?catalog={$catalogID}&&p={$i}'>{$i}</a></li>";
                        }
                    }
                    $next = $p + 1;                    
                    if(($next - 1) == $pageNo){
                        echo "<li class='disabled'><a><i class='material-icons'>chevron_right</i></a></li>";
                    }else{
                        echo "<li class='waves-effect'><a href='show.php?catalog={$catalogID}&&p=2'><i class='material-icons'>chevron_right</i></a></li>";
                    }
                    
                }

              echo "</ul>
                </div>";
  }

  function getCatalog($catalogID){
      showSlider();
      if($catalogID == 1){
          //$catalog = "";
          $catalog = "<span class='red-text'>Mountain</span>";
          $icon = "<i class='material-icons left medium'>filter_hdr</i>";
      }else if($catalogID == 2){
          $catalog = "<span class='red-text'>Sea</span>";
          $icon = "<i class='material-icons left medium'>pool</i>";
      }else if($catalogID == 3){
          $catalog = "<span class='red-text'>Night life</span>";
          $icon = "<i class='material-icons left medium'>location_city</i>";
      }else if($catalogID == 4){
          $catalog = "<span class='red-text'>Food</span>";
          $icon = "<i class='material-icons left medium'>local_dining</i>";
      }else{
          echo "<script>window.location.href = 'index.php';</script>";
      }
      $cataHead = strip_tags($catalog);
      echo "<script> document.title = \"Trip Or Trick: Category {$cataHead}\"</script>";

      $mysqli = new mysqli('localhost', 'homestead', 'secret', 'TripOrTrick');

      $offset = 6 * ($_GET['p'] - 1);


      $query = "SELECT COUNT(*) AS count FROM Reviews WHERE Reviews.Category={$catalogID}";
      $result = $mysqli->query($query);
      $count = $result->fetch_assoc();

      $query = "SELECT IF(temp.CountComment >= 1,temp.CountComment,0) as newCount, Reviews.ID, Members.Username,Members.ID AS MemID, Reviews.Review_topic, Reviews.Review_content, Reviews.Review_picture, DATE_FORMAT(Reviews.Review_date,'%d %M %Y') AS timedate FROM Reviews LEFT JOIN Members ON Reviews.Auther_ID = Members.ID LEFT JOIN  ( SELECT Comments.Review_ID, count(Comments.Comment_ID) AS CountComment FROM Comments LEFT JOIN Reviews ON Comments.Review_ID = Reviews.ID GROUP BY Comments.Review_ID) temp ON temp.Review_ID = Reviews.ID WHERE Category = {$catalogID} LIMIT 6 OFFSET {$offset};";
      
      $result = $mysqli->query($query);
      
      echo "<div class='row white'>";
      echo "<div class='col s12' style='padding-top:5px;'>{$icon}<h4>Category {$catalog}</h4></div>";
      echo "<div class='col s12'><hr>
              <div class='my-container center'>";
      if($result->num_rows == 0){
          echo "<div class='row grey-text'>ไม่มีข้อมูล</div>";
      }else{
          while($review = $result->fetch_assoc()){
          $content = categoryText($review['Review_content']);
          echo "<div class='row' style='padding-top:5px;'>
                      <div class='col s12 m3'><a href='show.php?page_id={$review['ID']}'><img class='responsive-img' src=\"{$review['Review_picture']}\"></a></div>
                      <div class='col s12 m9 left-align'>
                          <a href='show.php?page_id={$review['ID']}'>
                              <div class='row zero-m-bottom'><h5>{$review['Review_topic']}</h5></div>
                              <div class='row left-align'><p class='auto-newline'>{$content}</p></div>
                          </a>
                          <div class='row right-align zero-m-bottom'>                              
                              <span><i class='material-icons prefix'>account_circle</i><a href='show.php?usr={$review['MemID']}'>{$review['Username']}</a></span>
                              <span><i class='material-icons prefix'>comment</i> {$review['newCount']} comments</span>
                              <span><i class='material-icons prefix'>date_range</i>{$review['timedate']}</span>
                          </div>                          
                      </div>
                  </div><hr>";
          }
      }
      // pageination
      $pageNo = ceil($count['count'] / 6);
      echo"</div>
            </div>";
      showPageination($pageNo, $_GET['p'], $catalogID);

      echo "</div>
            </div>";
      $mysqli->close();
  }


  function showUser($id){
      //print_r($_SESSION);
      $mysqli = new mysqli('localhost', 'homestead', 'secret', 'TripOrTrick');
      $query = "SELECT ID, Picture, Username, Email, Name, Surname, DATE_FORMAT(Birthdate,'%d %M %Y') AS timedate, isAdmin, sex FROM Members WHERE ID = '{$id}'";
      if($result = $mysqli->query($query)){
          if($result->num_rows == 0)
            header("Location: index.php");
          $data = $result->fetch_assoc();
          if($data['Username'] == 'Deleted' and !empty($_SESSION))
            if($_SESSION['isAdmin'] == 0)
                header("Location: index.php");
          echo "<script> document.title = \"Trip Or Trick: {$data['Username']}\"</script>";

      echo "<div class='row grey darken-3 zero-m-bottom'><h5 class='white-text' style='margin:0px;padding-top:10px;padding-left:10px;padding-bottom:10px;'>ข้อมูลสมาชิก</h5></div>";
      echo "<div class='white'>
          <div class='row  blue-grey darken-4' style='padding-top:10px;'>
              <div class='col s12 m5 l3 center'>
                  <div class='white left-align' style='border-radius:10px;'>
                      <div class='row center' style='margin-bottom:10px;'>
                          <div class='row zero-m-bottom' style='padding-top:10px;'>
                              <img class='responsive-img circle' src='{$data['Picture']}' style='width:40%;'>
                              <h5 style='margin-top:0px;'><b class='red-text'>{$data['Username']}</b></h5>
                          </div>";
          if($data['isAdmin'] == 1){
                              echo "
                              <div class='row' style='margin:0px;'>
                                  <b class='red-text'>administrator</b>
                              </div>";
          }
                          
                      echo "</div>
                      <div class='row' style='margin-bottom:7px;'>
                          <div class='col s4'><b>Name</b></div>
                          <div class='col s8'>{$data['Name']}</div>
                      </div>
                      <div class='row' style='margin-bottom:7px;'>
                          <div class='col s4'><b>Surname</b></div>
                          <div class='col s8'>{$data['Surname']}</div>
                      </div>
                      <div class='row' style='margin-bottom:7px;'>
                          <div class='col s4'><b>Sex</b></div>
                          <div class='col s8'>{$data['sex']}</div>
                      </div>
                      <div class='row' style='margin-bottom:7px;'>
                          <div class='col s4'><b>Birthdate</b></div>
                          <div class='col s8'>{$data['timedate']}</div>
                      </div>
                      <div class='row' style='margin-bottom:7px;padding-bottom:5px;'>
                          <div class='col s4'><b>Email</b></div>
                          <div class='col s8'>{$data['Email']}</div>
                      </div><hr style='margin:10px;'>
                      <div class='row' style='margin-bottom:7px;padding-bottom:5px;'>";
                          if(isset($_SESSION) and !empty($_SESSION)){
                              if($_SESSION['id'] == $data['ID'] or $_SESSION['isAdmin'] == 1){
                                echo "<div class='col s12'><b>การตั้งค่า</b></div>
                                <div class='col s12'>
                                    <a id='editAccount' class='modal-trigger' href='#editAccountModal'>แก้ไขบัญชีผู้ใช้</a><br>
                                    <a id='editProfile' class='modal-trigger' href='#editProfileModal'>แก้ไขข้อมูลส่วนตัว</a>
                                </div>";
                              }
                              if($_SESSION['id'] != $data['ID']){
                                  if($data['Username'] != 'Deleted')
                                      echo "<div class='col s12'><a class='modal-trigger' href='#reportMember'>แจ้งลบผู้ใช้คนนี้</a></div>";
                              }                              
                          }
                          echo "
                          <div id='reportMember' class='modal modal-fixed-footer'>
                              <div class='modal-content'>
                              <div class='row'><i class='material-icons prefix medium left'>edit</i><b>แจ้งลบ <h4 class='red-text'>Member ID <span id='desID'></span></h4></b></div>                    
                                <h5>เหตุผล</h5>
                                <form action='send_report.php?desID={$data['ID']}&&selector=3' method='post'>
                                    <textarea name='reason' style='height:250px;resize: none;'></textarea>
                                    <button id='sendEdit3' type='submit' style='display:none;'></button>
                                </form>
                              </div>
                              <div class='modal-footer'>
                                <a  onclick=\"$('#sendEdit3').click();\" class='modal-action modal-close waves-effect waves-green btn-flat'><i class='red-text material-icons prefix left'>send</i>ส่ง</a>
                              </div>
                          </div>

                          <div id='editAccountModal' class='modal modal-fixed-footer'>
                              <div class='modal-content'>                                  
                                  <h4 style='padding-bottom:10px;'>แก้ไขบัญชีผู้ใช้</h4>
                                  <div class='row'>
                                      <h6>Username: <b class='red-text'>{$data['Username']}</b></h6>
                                  </div>
                                  <div class='row'>                          
                                      <form action='edit_profile.php?u={$id}&c=2' method='post'>
                                          <ul class='tabs'>
                                              <li class='tab col s3'><a class='active' href='#edit_username'>Change Username</a></li>
                                              <li class='tab col s3'><a href='#edit_password'>Change Password</a></li>
                                          </ul>
                                          <div id='edit_username' class='row'>
                                              <div class='row'>
                                                  <div class='input-field col l6'>
                                                      <i class='material-icons prefix'>account_circle</i>
                                                      <input id='editUsername' name='username' type='text' class='validate' length='20'>
                                                      <label>New Username</label>
                                                  </div>
                                              </div>
                                              <div class='row'>
                                                  <div class='input-field col l6'>
                                                      <i class='material-icons prefix'>vpn_key</i>
                                                      <input id='editPassword' name='pwd' type='password' class='validate' length='8'>
                                                      <label>Password</label>
                                                  </div>
                                                  <div class='input-field col l6'>
                                                      <i class='material-icons prefix'>vpn_key</i>
                                                      <input id='editPasswordCF' name='cf_pwd' type='password' class='validate' length='8'>
                                                      <label>Confirm Password</label>
                                                  </div>
                                              </div>
                                          </div>
                                          <div id='edit_password' class='row'>
                                              <div class='row'>
                                                  <div class='input-field col l6'>
                                                      <i class='material-icons prefix'>account_circle</i>
                                                      <input name='new_pwd' type='password' class='validate' length='8'>
                                                      <label>New Password</label>
                                                  </div>
                                              </div>
                                              <div class='row'>
                                                  <div class='input-field col l6'>
                                                      <i class='material-icons prefix'>vpn_key</i>
                                                      <input id='editPassword' name='old_pwd' type='password' class='validate' length='8'>
                                                      <label>Old Password</label>
                                                  </div>
                                                  <div class='input-field col l6'>
                                                      <i class='material-icons prefix'>vpn_key</i>
                                                      <input id='editPasswordCF' name='cf_old_pwd' type='password' class='validate' length='8'>
                                                      <label>Confirm Old Password</label>
                                                  </div>
                                              </div>
                                          </div>
                                          <button id='sendEdit2' type='submit' style='display:none;'></button>
                                      </form>
                                  </div>
                              </div>
                              <div class='modal-footer'>
                                  <a class='modal-action modal-close waves-effect waves-green btn-flat '>ยกเลิก</a>
                                  <a onclick=\"$('#sendEdit2').click();\" class='modal-action modal-close waves-effect waves-green btn-flat '>บันทึก</a>
                              </div>
                          </div>
                          <div id='editProfileModal' class='modal modal-fixed-footer'>
                              <div class='modal-content'>
                                  <h4 style='padding-bottom:10px;'>แก้ไขข้อมูลส่วนตัว</h4>
                                  <form action='edit_profile.php?c=1&u={$id}' method='post' enctype=\"multipart/form-data\">
                                      <div class='row'>
                                          <b>รูปประจำตัว</b>
                                          <input id=\"profilePic\" type=\"file\" name=\"proPic\">
                                      </div>
                                      <div class='row zero-m-bottom'>
                                          <div class='input-field col s6'>
                                                <input value='Name' id='editName' name='name' type='text' length='20'>
                                                <label class='active' for='editName'>Name</label>
                                          </div>
                                          <div class='input-field col s6'>
                                                <input value='Surname' id='editSurname' name='surname' type='text' length='20'>
                                                <label class='active' for='editSurname'>Surname</label>
                                          </div>
                                      </div>
                                      <div class='row'>
                                          <div style='padding-left:10px;'>
                                          <b>Sex</b><br>
                                            <input class='with-gap' type='radio' name='sex' value='Male' id='sexM' checked='checked' />
                                            <label for='sexM'>Male</label>
                                            <input class='with-gap' type='radio' name='sex' value='Female' id='sexF' />
                                            <label for='sexF'>Female</label>
                                          </div>
                                      </div>
                                      <div class='row'>
                                          <div class='input-field col s6'>
                                              <input type='date' id='editDatepick' name='bd' class='datepicker'>
                                              <label for='editDatepick'>Birthdate</label>
                                          </div>
                                          <div class='input-field col s6'>
                                              <input type='email' name='email' id='editMail' value='me@mail.com'>
                                              <label for='editMail'>Email</label>
                                          </div>
                                      </div>
                                      <div class='row'>
                                           <div class='input-field col s6'>
                                                <input value='' id='edit_pwd' name='pwd' type='password'>
                                                <label class='active' for='pwd'>Password</label>
                                            </div>
                                            <div class='input-field col s6'>
                                                <input value='' id='cf_pwd' name='cf_pwd' type='password'>
                                                <label class='active' for='cf_pwd'>Confirm Password</label>
                                            </div>
                                      </div>
                                      <button id='sendEdit' type='submit' style='display:none;'></button>
                                      <div id='editID' style='display:none;'>{$data['ID']}</div>
                                  </form>
                              </div>
                              <div class='modal-footer'>
                                <a class='modal-action modal-close waves-effect waves-green btn-flat '>ยกเลิก</a>
                                <a onclick=\"$('#sendEdit').click();\" id='editP' class='modal-action modal-close waves-effect waves-green btn-flat '>บันทึก</a>                                
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              <div class='col s12 m7 l9' > 
                  <div class='white center' style='border-radius:10px;'>
                      <div class='row zero-m-bottom'>
                          <i class='material-icons medium prefix'>explore</i><h5 style='margin-top:0px;padding-top:15px;'>การเคลื่อนไหวล่าสุด</h5>
                      </div>
                      <div class='row'>
                          <div style='padding-left:20px;padding-right:30px;'>
                              <h5 class='left-align' style='margin-top:0px;'>รีวิว</h5>";
                              $query = "SELECT ID, Review_topic, DATE_FORMAT(Review_date,'%d %M %Y %h:%i %p') AS review_date FROM Reviews WHERE Auther_ID = {$id} ORDER BY Review_date DESC";
                              $result = $mysqli->query($query);
                              while($row = $result->fetch_assoc()){
                                  echo "<div class='row left-align'>
                                            <div class='col s8'><a href='show.php?page_id={$row['ID']}' class='truncate'>{$row['Review_topic']}</a></div>                                  
                                            <div class='col s4'>เขียนเมื่อ {$row['review_date']}</div>
                                        </div>";
                              }                                                            
                          echo "</div>
                      </div>
                      <div class='row'>
                          <div style='padding-left:20px;padding-right:30px;'>
                              <h5 class='left-align' style='margin-top:0px;'>แสดงความคิดเห็น</h5>";
                              $query = "SELECT Comment_details,Review_topic, Reviews.ID, Review_ID, DATE_FORMAT(Comment_date,'%d %M %Y %h:%i %p') AS comment_date FROM Comments JOIN Reviews ON Comments.Review_ID = Reviews.ID WHERE Comment_auther = {$id} ORDER BY Comment_date DESC";
                              $result = $mysqli->query($query);                              
                              while($row = $result->fetch_assoc()){
                                  $details = htmlentities($row['Comment_details']);
                                  echo "<div class='row left-align'>
                                            <div class='col s8 truncate'>
                                              <div class='row zero-m-bottom'>
                                                  <div class='col s8 truncate'>
                                                      เขียน {$details}
                                                  </div>
                                                  <div class='col s4 truncate'>
                                                      ที่ <a href='show.php?page_id={$row['ID']}'>{$row['Review_topic']}</a>
                                                  </div>
                                              </div>
                                            </div>                                 
                                            <div class='col s4'>เขียนเมื่อ {$row['comment_date']}</div>
                                        </div>";
                              }                                                           
                          echo "</div>
                      </div>
                      
                  </div>
              </div>
          </div>
      </div>";
      }else{
        echo "Invalid Username";
      }
      $mysqli->close();
  }

  function checkToastStatus($status){
      if($_GET['status'] == 1){
          echo "<script> Materialize.toast('Log out successful', 4000); </script>";
      }else if($status == 2){
          echo "<script> Materialize.toast('Login successful', 4000); </script>";
      }else if($status == 3){
          echo "<script> Materialize.toast('Register successful', 4000); </script>";
      }else if($status == 4){
          echo "<script> Materialize.toast('Added Review', 4000); </script>";
      }else if($status == 5){
          echo "<script> Materialize.toast('Added Comment', 4000); </script>";
      }else if($status == 6){
          echo "<script> Materialize.toast('Invalid username or password!', 4000); </script>";
      }else if($status == 7){
          echo "<script> Materialize.toast('Sent Report', 4000); </script>";
      }else if($status == 8){
          echo "<script> Materialize.toast('Password must be same', 4000); </script>";
      }else if($status == 9){
          echo "<script> Materialize.toast('Select Only One Tab', 4000); </script>";
      }else if($status == 10){
          echo "<script> Materialize.toast('Please Fill All Fields', 4000); </script>";
      }else if($status == 11){
          echo "<script> Materialize.toast('Invalid Password', 4000); </script>";
      }else if($status == 12){
          echo "<script> Materialize.toast('Edited Profile', 4000); </script>";
      }else if($status == 13){
          echo "<script> Materialize.toast('Failed Query', 4000); </script>";
      }else if($status == 14){
          echo "<script> Materialize.toast('Deleted', 4000); </script>";
      }else if($status == 15){
          echo "<script> Materialize.toast('Set Admin', 4000); </script>";
      }else if($status == 16){
          echo "<script> Materialize.toast('Droped Admin', 4000); </script>";
      }else if($status == 17){
          echo "<script> Materialize.toast('Not Support Image Type <br>Or Image\'s Name Has Alreay Exist In Database', 4000); </script>";
      }
  }

  function showTools(){
    echo "<a href='#'><i id=\"al\" class=\"material-icons tooltipped\" data-position='down' data-delay='50' data-tooltip='Left align'>format_align_left</i></a>
                          <a href='#'><i id=\"ac\" class=\"material-icons tooltipped\" data-position='down' data-delay='50' data-tooltip='Center align'>format_align_center</i></a>
                          <a href='#'><i id=\"ar\" class=\"material-icons tooltipped\" data-position='down' data-delay='50' data-tooltip='Right align'>format_align_right</i></a>                        
                          <a href='#'><i id=\"li\" class=\"material-icons tooltipped\" data-position='down' data-delay='50' data-tooltip='Insert link'>insert_link</i></a>
                          <a href='#'><i id=\"pic\" class=\"material-icons tooltipped\" data-position='down' data-delay='50' data-tooltip='Insert picture'>perm_media</i></a>
                          <a href='#'><i id=\"vid\" class=\"material-icons tooltipped\" data-position='down' data-delay='50' data-tooltip='Insert video'>music_video</i></a>
                          <a href='#'><i id=\"b\" class=\"material-icons tooltipped\" data-position='down' data-delay='50' data-tooltip='Bold'>format_bold</i></a>
                          <a href='#'><i id=\"underl\" class=\"material-icons tooltipped\" data-position='down' data-delay='50' data-tooltip='Underline'>format_underline</i></a>
                          <a href='#'><i id=\"nl\" class=\"material-icons tooltipped\" data-position='down' data-delay='50' data-tooltip='New line'>vertical_align_bottom</i></a>
                          <a href='#'><i id=\"ital\" class=\"material-icons tooltipped\" data-position='down' data-delay='50' data-tooltip='Italic'>format_italic</i></a>
                          <a href='#'><i id=\"quote\" class=\"material-icons tooltipped\" data-position='down' data-delay='50' data-tooltip='Quote text'>format_quote</i></a>                      

                          ";
  }

  function checkEdit($reviewID, $user){
      $mysqli = new mysqli('localhost', 'homestead', 'secret', 'TripOrTrick');
      $query = "SELECT EXISTS(SELECT * FROM Reviews WHERE Auther_ID = {$user} AND ID = {$reviewID})";
      if($result = $mysqli->query($query)){
          if ($result->num_rows > 0 OR $_SESSION['isAdmin'] == 1) {
              $query = "SELECT Review_topic, Review_content, Review_picture, Category, Members.Username FROM Reviews JOIN Members ON Reviews.Auther_ID = Members.ID WHERE Reviews.ID = {$reviewID}";
            if($result = $mysqli->query($query)) {
              $row = $result->fetch_assoc();
              $content = unTags($row['Review_content']);
              echo "
                <div class=\"row right grey-text\">
                  ผู้เขียนรีวิว <i class='material-icons left'>account_circle</i><span class='red-text'>{$row['Username']}</span>
              </div>
              <br>
              <form id='wReview' method=\"post\" action=\"send_review.php?id={$reviewID}\" enctype=\"multipart/form-data\">
               <div class=\"row\">
                  <div class=\"input-field col s12\">
                      <i class=\"material-icons prefix\">stars</i>
                      <input id=\"topic\" type=\"text\" name=\"topic\" class=\"validate\" value='{$row['Review_topic']}' required>
                      <label for=\"topic\">Topic</label>
                  </div>
                  <!-- TOOLs -->
                  <div class=\"row center-align\" style=\"margin-bottom: 0px;\">
                      <div class=\"col s12\" style=\"padding-top: 10px;\">";

                      showTools();
                      echo "</div>
                    </div>
                  <div class=\"input-field col s12\">
                      <i class=\"material-icons prefix\">mode_edit</i>
                      <textarea id=\"content\" class=\"materialize-textarea\" name=\"content\" length=\"10240\" required>{$content}</textarea>
                      <label for=\"content\">เขียน Review ของคุณที่นี่เลย!</label><br><br>

                      <!-- Ratio Button -->
                      <div class=\"col s12 grey lighten-3 left-align\" style=\"margin-bottom:10px;\">
                        <div class=\"row\">
                          <p class=\"grey-text\" style=\"padding-left:10px;\"><b>Catalog</b></p>
                            <p>";
                            if($row['Category'] == 1)
                              echo "<div class=\"col s6 l3\"><input class=\"with-gap\" checked=\"checked\" name=\"catalog\" type=\"radio\" id=\"mountain\" value=\"1\">
                                <label for=\"mountain\">Mountain</label></div>";
                            else
                                echo "<div class=\"col s6 l3\"><input class=\"with-gap\" name=\"catalog\" type=\"radio\" id=\"mountain\" value=\"1\">
                                <label for=\"mountain\">Mountain</label></div>";
                            
                            if($row['Category'] == 2)
                              echo "<div class=\"col s6 l3\">
                                <input class=\"with-gap\" name=\"catalog\" checked=\"checked\" type=\"radio\" id=\"sea\" value=\"2\">
                                <label for=\"sea\">Sea</label>
                            </div>";
                            else
                              echo "<div class=\"col s6 l3\">
                                <input class=\"with-gap\" name=\"catalog\" type=\"radio\" id=\"sea\" value=\"2\">
                                <label for=\"sea\">Sea</label>
                            </div>";

                            if($row['Category'] == 3)
                              echo "<div class=\"col s6 l3\">
                                <input class=\"with-gap\" name=\"catalog\" checked=\"checked\" type=\"radio\" id=\"night\" value=\"3\">
                                <label for=\"night\">Night Life</label>
                            </div>";
                            else
                              echo "<div class=\"col s6 l3\">
                                <input class=\"with-gap\" name=\"catalog\" type=\"radio\" id=\"night\" value=\"3\">
                                <label for=\"night\">Night Life</label>
                            </div>";

                            if($row['Category'] == 4)
                            echo "<div class=\"col s6 l3\">                                
                                <input class=\"with-gap\" name=\"catalog\" checked=\"checked\" type=\"radio\" id=\"food\" value=\"4\">
                                <label for=\"food\">Food</label>  
                            </div>";
                            else
                              echo "<div class=\"col s6 l3\">                                
                                <input class=\"with-gap\" name=\"catalog\" type=\"radio\" id=\"food\" value=\"4\">
                                <label for=\"food\">Food</label>  
                            </div>";
                          
                          echo "</p>
                        </div>
                      </div>
                  </div>
                  <div class=\"col s12\"><br>
                    <button type=\"submit\" class=\"btn red large\" value=\"edit\" name='submitBtn'>Edit</button> 
                  </div>                                  
                </div>
          </form>";
            } else{
              echo "failed query";
            }
          }else{
              header("Location: show.php?page_id={$reviewID}");
          }
          
              
      }else{
          echo "error query";
      }
      $mysqli->close();
  }
  function defualtWrite(){
      echo "
      <div class=\"row right grey-text\">
          ผู้เขียนรีวิว <i class='material-icons left'>account_circle</i><span class='red-text'>{$_SESSION['user']}</span>
      </div>
      <br>
      <form id='wReview' method=\"post\" action=\"send_review.php\" enctype=\"multipart/form-data\">
               <div class=\"row\">
                  <div class=\"input-field col s12 left-align\">
                      <i class=\"material-icons prefix\">stars</i>
                      <input id=\"topic\" type=\"text\" name=\"topic\" class=\"validate\" required>
                      <label for=\"topic\">Topic</label>
                  </div>
                  <!-- TOOLs -->
                  <div class=\"row center-align\" style=\"margin-bottom: 0px;\">
                      <div class=\"col s12\" style=\"padding-top: 10px;\">";
                      showTools();
                          
                      echo "</div>
                    </div>
                  <div class=\"input-field col s12\">
                      <i class=\"material-icons prefix\">mode_edit</i>
                      <textarea id=\"content\" class=\"materialize-textarea\" name=\"content\" length=\"10240\" required></textarea>
                      <label for=\"content\">เขียน Review ของคุณที่นี่เลย!</label><br><br>
                      <div class=\"col s12 left-align \" style=\"margin:20px;\">
                          <div class=\"col s3\">รูปภาพหน้าปก</div>
                          <div class=\"col s9\"><input type=\"file\" name=\"cover-img\" id=\"img-upload\" accept=\"image/*\"></div>
                      </div>
                      <!-- Ratio Button -->
                      <div class=\"col s12 grey lighten-3 left-align\" style=\"margin-bottom:10px;\">
                        <div class=\"row\">
                          <p class=\"grey-text\" style=\"padding-left:10px;\"><b>Catalog</b></p>
                            <p>
                            <div class=\"col s6 l3\">
                                <input class=\"with-gap\" checked=\"checked\" name=\"catalog\" type=\"radio\" id=\"mountain\" value=\"1\">
                                <label for=\"mountain\">Mountain</label>
                            </div>
                            <div class=\"col s6 l3\">
                                <input class=\"with-gap\" name=\"catalog\" type=\"radio\" id=\"sea\" value=\"2\">
                                <label for=\"sea\">Sea</label>
                            </div>
                            <div class=\"col s6 l3\">
                                <input class=\"with-gap\" name=\"catalog\" type=\"radio\" id=\"night\" value=\"3\">
                                <label for=\"night\">Night Life</label>
                            </div>
                            <div class=\"col s6 l3\">                                
                                <input class=\"with-gap\" name=\"catalog\" type=\"radio\" id=\"food\" value=\"4\">
                                <label for=\"food\">Food</label>  
                            </div>
                          </p>
                        </div>
                      </div>
                  </div>
                  <div class=\"col s12\"><br>
                    <button type=\"submit\" class=\"btn green large\" value=\"send\" name='submitBtn'>Send</button> 
                  </div>                                  
                </div>
          </form>";
  }

?>
