<!DOCTYPE html>
<html>
<head>
	<title>Trip Or Trick :: Write Review</title>
	    <!--Import Google Icon Font-->
    <link href='https://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet' type='text/css'>
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
    <link rel="shortcut icon" href="imgs/world.ico">
    <!-- Bootstrap 
    <link href="css/bootstrap.min.css" rel="stylesheet">-->
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <div class="loader"></div>
</head>
<body class="blue-grey darken-4">
	<!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
	  <script src="js/scripts.js"></script>

  <?php 
        session_start();
        if(!isset($_SESSION['user']))
            header("Location: index.php"); 
        include('phpScripts.php');
    ?>
	<!-- start -->
	<div class="my-container white center"><br>
      <i class='material-icons large'>mode_edit</i>
      <h3>เขียนรีวิว</h3><hr style="width:90%;">
      <div class="container">
          <?php
              if (isset($_GET['p']) and !empty($_GET['p'])){
                  checkEdit($_GET['p'], $_SESSION['id']);
              }else{
                  defualtWrite();
              }
          ?>
		      
		      <br>
      </div>
	</div>

	<!-- Fixed control button -->
   <?php showFixedBtn();showFooter(); ?>
</body>
</html>