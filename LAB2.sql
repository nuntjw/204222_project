#1.
SELECT Physician.Name FROM Undergoes
JOIN Physician
ON Undergoes.Physician = Physician.EmployeeID
WHERE Undergoes.Procedures NOT IN
(	SELECT Undergoes.Procedures
	FROM Undergoes
	LEFT JOIN Trained_In
	ON Undergoes.Physician = Trained_In.Physician
	WHERE Undergoes.Procedures = Trained_In.Treatment
)

#2
SELECT Physician.Name AS Physician_Name, Procedures.Name AS Procedure_Name, Undergoes.DateUndergoes AS Undergoes_Date, Patient.Name AS Patient_Name FROM Undergoes
JOIN Physician
ON Undergoes.Physician = Physician.EmployeeID
JOIN Procedures
ON Undergoes.Procedures = Procedures.Code
JOIN Patient ON Undergoes.Patient = Patient.SSN
WHERE Undergoes.Procedures NOT IN
(	SELECT Undergoes.Procedures
	FROM Undergoes
	LEFT JOIN Trained_In
	ON Undergoes.Physician = Trained_In.Physician
	WHERE Undergoes.Procedures = Trained_In.Treatment
)

#7
SELECT Patient.Name, COUNT(Prescribes.Patient) AS Prescribed_Count
FROM Prescribes
JOIN Patient ON Prescribes.Patient = Patient.SSN
GROUP BY Patient.Name

#8
SELECT Patient.Name, temp.Prescribed_Count FROM Prescribes
JOIN Patient ON Prescribes.Patient = Patient.SSN
JOIN
(	
    SELECT Patient.Name, COUNT(Prescribes.Patient) AS Prescribed_Count
	FROM Prescribes
 	JOIN Patient ON Prescribes.Patient = Patient.SSN
	GROUP BY Patient.Name
) AS temp
ON temp.Name = Patient.Name
WHERE temp.Prescribed_Count < 2